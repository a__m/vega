function generatedFig = prepare_contour_2d(lowerBound, upperBound, fitnessFunctions, fig)
    generatedFig = fig;
    for fun = fitnessFunctions
        startpoint = 2*(max(upperBound)-min(upperBound));
        maximum = max([abs(lowerBound) abs(upperBound)])*1.3;
        draw_contour(-maximum+startpoint, maximum, (max(upperBound)-min(lowerBound))*.1, fun, fig);
        fig = fig + 1;
        draw_contour(-maximum+startpoint, maximum, (max(upperBound)-min(lowerBound))*.1, fun, fig);
        fig = fig + 1;
    end
    generatedFig = fig - generatedFig;
end

