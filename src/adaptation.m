function adapt = adaptation(population, fitnessFunctions, constraints, nrOfVar, k)
    adapt = [];
    for p = population
        [adaptValue, punish] = adaptFunction(p, fitnessFunctions, constraints, k);
        adapt = [adapt; [adaptValue punish p']];
    end
end