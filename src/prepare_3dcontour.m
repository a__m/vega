function prepare_3dcontour(lowerBound, upperBound, fitnessFunctions, fig)
    for fun = fitnessFunctions
       draw_3dcontour(min(lowerBound), max(upperBound), (max(upperBound)-min(lowerBound))*.1, fun, fig);
       fig = fig + 1;
    end
end

