function accepted_population = filterout_wrong(finalPopulation, constraints)

    % filter constraints 
    accepted_population = [];
    for x = finalPopulation
        if find(isnan(x), 1)
            continue;
        end
        
        % don't choose the same arguments
        if ~isempty(accepted_population)
            indx=ismembertol(x,accepted_population, 10^-10);
            if all(indx)
                continue; 
            end
        end
        allConstraintsPassed = true;
        
        for constraint = constraints
            if (~eval(char(constraint)))
                allConstraintsPassed = false;
                break;
            end
        end
        
        if allConstraintsPassed
           accepted_population = [accepted_population x]; 
        end
    end


end

