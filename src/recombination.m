function children = recombination(population, pc)
    nrOfRows = length(population(:,1));
    nrOfColumns = length(population(1,:));
    
    children = zeros(nrOfRows, nrOfColumns);
    for dimension = 1:nrOfRows
        pop = population(dimension, :);
        child = zeros(1, nrOfColumns);
        childIndex = 1;
        while length(pop) >= 2
            [parent1 pop] = getRandomParent(pop);
            [parent2 pop] = getRandomParent(pop);
            parents = [parent1 parent2];
            if rand(1) <= pc
                crossed = cross(parents);
                child(childIndex) = crossed(1);
                child(childIndex+1) = crossed(2);
            else
                child(childIndex) = parents(1);
                child(childIndex+1) = parents(2);
            end
            childIndex = childIndex + 2;
        end
        children(dimension, :) = child;
    end    
end

function [parent population] = getRandomParent(population)
    randParentIndex = randi([1 length(population)], 1);
    parent = population(randParentIndex);
    population(randParentIndex) = [];
end

function children = cross(parents)
    parent1 = dec2bin(typecast(single(parents(1)),'uint32'),32);
    parent2 = dec2bin(typecast(single(parents(2)),'uint32'),32);
    crossingFrame1 = randi([1 29], 1);
    crossingFrame2 = randi([crossingFrame1 30], 1);
    crossingFrame3 = randi([crossingFrame2 31], 1);
    Lp = length(parent1);
    child1(1:Lp) = '0';
    child2(1:Lp) = '0';
    for i = 1:Lp
        if (i <= crossingFrame1)
            child1(i) = parent2(i);
            child2(i) = parent1(i);
        elseif (i > crossingFrame1 && i <= crossingFrame2)
            child1(i) = parent1(i);
            child2(i) = parent2(i); 
        elseif (i > crossingFrame2 && i <= crossingFrame3)
            child1(i) = parent2(i);
            child2(i) = parent1(i);
        else
            child1(i) = parent1(i);
            child2(i) = parent2(i);     
        end
    end
    children = [double(typecast(uint32(bin2dec(child1)),'single')) double(typecast(uint32(bin2dec(child2)),'single'))];
end
