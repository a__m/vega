function draw_3dcontour(xstart, xend, dx, fun, figureNr)
    xi = xstart:dx:xend;
    xi = fliplr(xi);
    [X,Y] = meshgrid(xi);
    F = zeros(length(X), length(X(1,:)));
    for i = 1:length(X)
        for j = 1:length(X(1,:))
            x = [X(i, j), Y(i, j)];
            F(i, j) = eval(char(fun));
        end
    end

    rotate3d on;
    figure(figureNr);
    meshc(X, Y ,F);
end

