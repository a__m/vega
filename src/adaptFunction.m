function [fgoal, punish] = adaptFunction(x, fitnessFunctions, constraints, k)
    punish = 0;
    for i = length(x)
        if (x(i) == NaN || x(i) == Inf || x(i) == -Inf) 
            fgoal = Inf;
            return
        end
    end
    
    if isempty(constraints)
        fun = fitnessFunctions(k);
        fgoal = eval(char(fun));
    else
        fun = fitnessFunctions(k);
        fgoal = eval(char(fun));
        for constraint = constraints
            if (~eval(char(constraint)))
               punish = fgoal * 10 + 1000;
            end
        end
    end
    
end