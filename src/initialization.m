function basePopulation = initialization(L, l, u, nrOfVariables)
    basePopulation = zeros(nrOfVariables, L);
    for i = 1:nrOfVariables
        basePopulation(i,:) = l(i) + (u(i)-l(i)) * rand(1, L);
    end
end