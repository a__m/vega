function hatch_area(lowerBound, upperBound, fig, nrOfDiagrams)
    for i = 1:nrOfDiagrams/2
        % kreskowanie wzgl?dem x
        half_x_axis_x = lowerBound(1):(upperBound(1)-lowerBound(1))/20:upperBound(1);
        xaxis_x = [half_x_axis_x; half_x_axis_x];
        xaxis_x = xaxis_x(:).';

        y1 = lowerBound(2) * ones(1, length(half_x_axis_x));
        y2 = upperBound(2) * ones(1, length(half_x_axis_x));
        yaxis_x = [y1; y2];
        yaxis_x = yaxis_x(:).';

        figure(fig);
        hold on;
        for i = 1:2:length(xaxis_x)-1
            plot(xaxis_x(i:i+1), yaxis_x(i:i+1), 'Color', [.5 .5 .5]);
        end
        hold off;

        fig = fig + 1;
        figure(fig);
        hold on;
        for i = 1:2:length(xaxis_x)-1
            plot(xaxis_x(i:i+1), yaxis_x(i:i+1), 'Color', [.5 .5 .5]);
        end
        hold off;
        fig = fig + 1;
    end
end

