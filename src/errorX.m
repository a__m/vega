function populationChanged = errorX(population1, population2)    
    if isempty(population2)
        populationChanged = 1;
        return;
    end
    NaNIndexes = isnan(population1);
    population1(NaNIndexes,:) = 0;
    NaNIndexes = isnan(population2);
    population2(NaNIndexes,:) = 0;
%     
    populationChanged = any(~ismembertol(population1, population2, 0.1, 'ByRows', true));
    
    if ~populationChanged
        fprintf('Populacja sie nie zmienila, koniec\n');
    end
end