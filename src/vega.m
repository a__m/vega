% L - liczba iteracji
% Lp - liczba osobnikow
% pc - prawdopodobienstwo krzyzowania
% pm - prawdopodobienstwo mutacji
% A - zbior rozwiazan niezdominowanych
function [A, finalPopulation] = vega(L, Lp, pc, pm, lowerBound, upperBound, fitnessFunctions, constraints, nrOfVar, maximize) 
    k = length(fitnessFunctions);
    firstPopulation = initialization(Lp, lowerBound, upperBound, nrOfVar);
    basePopulation = firstPopulation;
    finalPopulation = [];
    currentL = 1;
    allSameGoalsBetweenGenerationIJ = false;
    bestFxs = zeros(k, 1);
    oldAdaptedAll = [];
    while currentL <= L && errorX(basePopulation, finalPopulation) && ~allSameGoalsBetweenGenerationIJ
        fprintf('Current L: %d\n', currentL);
        if ~isempty(finalPopulation)
            basePopulation = finalPopulation;
        end
        
        selected = [];
        adaptedAll = zeros(Lp, nrOfVar+2, k); % 2, because goal and punischment also
        for i=1:k
            adapt = adaptation(basePopulation, fitnessFunctions, constraints, nrOfVar, i);
            adaptedAll(:,:,i) = adapt;
            selected = [selected selection(adapt, k, nrOfVar, maximize)];
        end
        % usuwanie tych co maja kare
        %adapted_all(adapted_all(:,2,:) > 0,:,:) = [];
        
        if ~isempty(oldAdaptedAll)
            oldGoals = reshape(oldAdaptedAll(:,1,:), [length(oldAdaptedAll(:,1)), length(oldAdaptedAll(1,1,:))]);
        else
            oldGoals = [];
        end
        
        % jesli mamy stare rozwiazania to przerob je na wielowymiarowa
        % tablice wartosci celu
        newGoals = reshape(adaptedAll(:,1,:), [length(adaptedAll(:,1)), length(adaptedAll(1,1,:))]);
        allSameGoalsBetweenGenerationIJ = errorFX(oldGoals, newGoals);
        if allSameGoalsBetweenGenerationIJ
           break 
        end
        oldAdaptedAll = adaptedAll;
        
        row = adaptedAll(:,2,:) ~= 0;
        adaptedAll(row(:,:,1), :, :) = [];

        secondPopulation = recombination(selected, pc);
        finalPopulation = mutation(secondPopulation, pm);
        color = [currentL/(1.05*L) currentL/(1.05*L) currentL/(1.1*L)];
        currentL = currentL + 1;

        % dodaj punkty do warstwic
        if nrOfVar == 2
            xf1_p = adaptedAll(:,end-1,1);
            yf1_p = adaptedAll(:,end,1);

            figure(30);
            hold on;
            plot(xf1_p, yf1_p, '*', 'Color', color);
            hold off;
            xlabel('x(1)');
            ylabel('x(2)');
            title('f1: Warstwice, obszar poszukiwa?, kolejne iteracje x');

            figure(32);
            hold on;
            plot(xf1_p, yf1_p, '*', 'Color', color);       
            hold off;
            xlabel('x(1)');
            ylabel('x(2)');
            title('f2: Warstwice, obszar poszukiwa?, kolejne iteracje x');
           
        end
    end

    finalPopulation = filterout_wrong(finalPopulation, constraints);
    A = select_notdominated(finalPopulation, fitnessFunctions);
end
