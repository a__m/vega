function allSame = errorFX(fitnessOld, fitnessNew)    
    allSame = false;
    if isempty(fitnessOld)    
        return;
    end
    if all(abs(fitnessOld - fitnessNew) <= 0.001)
        allSame = true;
    else
        allSame = false;
    end

    if allSame
        fprintf('Funkcja celu nie zmienila sie koniec\n');
    end
end