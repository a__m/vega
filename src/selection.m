function selected = selection(adapt, k, nrOfVar, maximize)
   % sum of first and second column as first new column
    adaptWithPunishment = [sum(adapt(:,1:2),2) adapt(:,3:end)];
    N = length(adaptWithPunishment(:,1));
    if maximize
        sorted_population = -sortrows(-adaptWithPunishment);
    else
        sorted_population = sortrows(adaptWithPunishment);
    end
   
    selected = [];
    
    % remove infinity, is don't have sense to compute it
    InfIndexes = sorted_population(:,1) == Inf;
    NaNIndexes = isnan(sorted_population(:,1));
    sorted_population(InfIndexes | NaNIndexes,:) = [];
    sumOfGoals = sum(sorted_population(:,1));
    probablityOfElement = sorted_population(:,1)/sumOfGoals;
    przedzial = [0; cumsum(probablityOfElement)];
    przedzial = fliplr(przedzial')';
    p = rand(N/k,1);
    for px = 1:length(p) 
        x = p(px);
        for i = 1:length(probablityOfElement)               
           if x <= przedzial(i) && x >= przedzial(i+1)
               selected = [selected sorted_population(i,end-nrOfVar+1:end)'];
               break;
           end
        end 
    end
end