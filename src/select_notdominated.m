function A = select_notdominated(accepted_population, fitnessFunctions)
    fitnessX = [];
    
    fitnessX = zeros(length(accepted_population(1,:)), length(fitnessFunctions));   
    for x = 1:length(accepted_population(1,:))
        for i = 1:length(fitnessFunctions);
            % constrainty nie maja tu znaczenia gdyz populacja ma tylko
            % poprawne rozwiazania
            [fitnessX(x, i),~] = adaptFunction(accepted_population(:,x), fitnessFunctions, '', i);
        end
    end
    
    popx = 1;
    A = [];

    for row = 1:length(fitnessX(:,1))
        fitness = fitnessX(row,:);
        dominated = false;
        for r = 1:length(fitnessX(:,1))
            f = fitnessX(r,:);

            if f == fitness
                continue;
            end
            
            dominated_part = 0;
            for i = 1:length(fitnessFunctions)
                if(f(i) < fitness(i))
                    dominated_part = dominated_part + 1;
                end
            end
            
            if dominated_part == length(fitnessFunctions)
                dominated = true;
                break;
            end
        end
        if dominated == false
            A = [A accepted_population(:,popx)];   
        end
        popx = popx + 1;
    end
end