function mutatedPopulation = mutation(population, pm)
    mutatedPopulation = [];
    nrOfColumns = length(population(1,:));
    for dimension = 1:length(population(:,1))
        mutated = zeros(1, nrOfColumns);
        mutatedIndex = 1;
        for p = population(dimension,:)
            pbin = dec2bin(typecast(single(p),'uint32'),32);
            for i = [1:1:25, 32]
                if rand(1) < pm
                    if pbin(i) == '1'
                        pbin(i) = '0';
                    else
                        pbin(i) = '1';
                    end
                end
            end
            mutated(mutatedIndex) = double(typecast(uint32(bin2dec(pbin)),'single'));
            mutatedIndex = mutatedIndex + 1;
        end

        mutatedPopulation = [mutatedPopulation; mutated];
    end
end