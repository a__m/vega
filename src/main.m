clear all; close all;

L = 100; % liczba iteracji
Lp = 4; % liczba osobnikow
pc = 0.98;
pm = 0.08;

%%% Numery obrazkow
% 10 - kolejne iteracje xi
% 11 - f(xi)
% 12 - wartosc kryterium stopu
% 20 - optymalne x*
% 21 - f(x*)
% 22 - wartosc kryterium stopu
% 30 - wartwice f1, zakreskowane X, punkty w iteracjach x -> done
% 31 - wartwice f1, zakreskowane X, x* -> done
% 32 - wartwice f2, zakreskowane X, punkty w iteracjach x -> done
% 33 - wartwice f2, zakreskowane X, x* -> done
% 34 - wartwice 3d f1, x*, f(x*) -> done
% 35 - warstwice 3d f2, x*, f(x*) -> done
% 40 - pareto -> done
% 41 - zbi?r rozwiazan koncowych -> done
firstFigureNrForContour = 30;
firstFigureNrFor3d = 34;

%%% Schaffer ujemny
% fitnessFunctions = {'x(1)^2-4', '(x(1)-2)^2-6'};
% constraints = {'x < 10', 'x > -10'};
% lowerBound = [-10];
% upperBound = [10];

%%% Binh and Korn function:	
% fitnessFunctions = {'4*x(1)^2 + 4*x(2)^2', '(x(1)-5)^2 + (x(2) - 5)^2'};
% constraints = {'(x(1)-5)^2 + x(2)^2 <= 25', '(x(1)-8)^2+(x(2)+3)^2>= 7.7', 'x(1) >= 0', 'x(1) <= 5', 'x(2) >= 0', 'x(2) <= 3'};
% lowerBound = [0 0];
% upperBound = [5 3];

%%% Chakong and Haimes
% fitnessFunctions = {'2 + (x(1) - 2)^2 + (x(2) - 1)^2', '9 * x(1) - (x(2) - 1)^2'};
% constraints = {'x(1)^2 + x(2)^2 <= 225', 'x(1) - (3 * x(2)) + 10 <= 0', 'x(1) <= 20', 'x(2) <= 20', 'x(1) >= -20', 'x(2) >= -20'};
% lowerBound = [-20 -20];
% upperBound = [20 20];

%%% Test function 4.
fitnessFunctions = {'x(1)^2 - x(2)', '-0.5 * x(1) - x(2) -1'};
constraints = {'6.5 - x(1)/6 - x(2) >= 0', '7.5 - 0.5 * x(1) - x(2) >= 0', '30 - 5 * x(1) - x(2) >= 0', 'x(1) >= -7', 'x(1) <= 4', 'x(2) >= -7', 'x(2) <= 4'};
lowerBound = [-7 -7];
upperBound = [4 4];

nrOfVar = length(lowerBound);

if nrOfVar == 2
    % przygotowanie obraz?w pod wartwice

    nrOfDiagrams = prepare_contour_2d(lowerBound, upperBound, fitnessFunctions, firstFigureNrForContour);
    drawnow;
    % zakreskuj warstwice
    hatch_area(lowerBound, upperBound, firstFigureNrForContour, nrOfDiagrams);
    drawnow;
    % przygotowanie 3d dla funkcji celu
    prepare_3dcontour(lowerBound, upperBound, fitnessFunctions, firstFigureNrFor3d);
    drawnow;
end

[final, all] = vega(L, Lp, pc, pm, lowerBound, upperBound, fitnessFunctions, constraints, nrOfVar, false);
fitnesses = [];

figure(40);
hold on;
for x = final
    plot(eval(char(fitnessFunctions(1))),  eval(char(fitnessFunctions(2))), '*');
    xlabel('f1(x)');
    ylabel('f2(x)');
    title('Wykres niezdominowanych rozwiazan Pareto');
end
hold off;

figure(41);
hold on;
for x = all
    plot(eval(char(fitnessFunctions(1))),  eval(char(fitnessFunctions(2))), '*');
    xlabel('f1(x)');
    ylabel('f2(x)');
    title('Wykres zdominowanych rozwiazan wszystkich Pareto 2');
end
hold off;

if nrOfVar == 2
    figure(31);
    hold on;
    for x = final
        plot(x(1), x(2), '*');
    end
    hold off;
    xlabel('x(1)');
    ylabel('x(2)');
    title('f1: Warstwice, obszar poszukiwa?, x*');

    figure(33);
    hold on;
    for x = final
        plot(x(1), x(2), '*');
    end
    hold off;
    xlabel('x(1)');
    ylabel('x(2)');
    title('f2: Warstwice, obszar poszukiwa?, x*');

    figure(34);
    hold on;
    for x = final
        x
        v = [eval(char(fitnessFunctions(1))),  eval(char(fitnessFunctions(2)))]
        plot3(x(1), x(2), v(1),  '*');
    end
    rotate3d on;
    hold off;
    xlabel('x(1)');
    ylabel('x(2)');
    title('f1: Wartwice 3D, x*, f1(x*)');

    figure(35);
    hold on;
    for x = final
        v = [eval(char(fitnessFunctions(1))),  eval(char(fitnessFunctions(2)))];
        plot3(x(1), x(2), v(2),  '*');
    end
    rotate3d on;
    hold off;
    xlabel('x(1)');
    ylabel('x(2)');
    title('f2: Warstwice 3D, x*, f2(x*)');
end
